# zohnmall

#### 介绍
ZohnMall是一个综合性的B2C电商平台，以微服务架构开发的平台为用户提供更友好的体验。

#### 技术栈
1.  后台管理系统：renrenfast快速搭建框架
2.  后端：SpringBoot、SpringCloudAlibaba，Mybatis-Plus，MySQL，Redis，Nginx，RabbitMQ、Redisson、Nacos等

#### 实现功能
1.  使用Nacos作为注册中心与配置中心，借助Nginx负载均衡到Gateway，微服务之间使用Fegin进行远程调用；
2.  使用Redis缓存商品分类、购物车、session等信息；
3.  使用Redisson分布式锁以保证缓存一致性的问题，使用Seata解决分布式事务问题；
4.  使用Spring Session实现了Oauth2码云社交登录，单点登录，短信验证码等功能；
5.  使用RabbitMQ完成订单提交功能的分布式事务问题；
6.  使用ElasticSearch完成商品搜索功能；

#### 安装教程

1.  编写配置文件
2.  导入数据库文件

#### 部分演示截图
![输入图片说明](%E9%A6%96%E9%A1%B5.PNG)
![输入图片说明](%E8%B4%AD%E7%89%A9%E8%BD%A6.PNG)


