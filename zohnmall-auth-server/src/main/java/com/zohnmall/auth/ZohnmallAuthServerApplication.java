package com.zohnmall.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * SpringSession核心原理
 * 1）、EnableRedisHttpSession导入RedisHttpSessionConfiguration配置
 *  1、给容器中添加了一个组件
 *      RedisOperationsSessionRepository：redis操作session。session的增删改查封装类
 *  2、SessionRepositoryFilter ==》 Filter: session存储过滤器，每个请求过来都必须经过filter
 *
 */

@EnableRedisHttpSession
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class ZohnmallAuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallAuthServerApplication.class, args);
    }

}
