package com.zohnmall.auth.controller;

import com.alibaba.fastjson.TypeReference;
import com.zohnmall.auth.feign.MemberFeignService;
import com.zohnmall.auth.feign.ThirdPartyFeignService;
import com.zohnmall.auth.vo.UserLoginVo;
import com.zohnmall.auth.vo.UserRegistVo;
import com.zohnmall.common.constant.AuthServerConstant;
import com.zohnmall.common.exception.BizCodeEnum;
import com.zohnmall.common.utils.R;
import com.zohnmall.common.vo.MemberRespVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Controller
public class LoginController {
    @Autowired
    ThirdPartyFeignService thirdPartyFeignService;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    MemberFeignService memberFeignService;
    /**
     * 发送请求直接跳转到一个页面
     * SpringMVC viewcontroller： 将请求和页面映射过来
     */

    /**
     * 发送验证码
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/sms/sendcode")
    public R sendCode(@RequestParam("phone") String phone) {
        // TODO 1、接口防刷
        String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);
        if (redisCode != null) {
            long l = Long.parseLong(redisCode.split("_")[1]);
            if (System.currentTimeMillis() - l < 60000) {
                // 60秒内不能再发
                return R.error(BizCodeEnum.SMS_CODE_EXCEPTION.getCode(), BizCodeEnum.SMS_CODE_EXCEPTION.getMsg());
            }
        }
        // 2、验证码校验.redis存key-phone，value-code
        String code = String.valueOf(new Random().nextInt(999999));
        if (code.length() < 6) {
            for (int i = 0; i < 6 - code.length(); i++) {
                code = "0" + code;
            }
        }
        thirdPartyFeignService.sendCode(phone, code);
        code += "_" + System.currentTimeMillis();
        // redis缓存验证码，10分钟过期，防止同一个手机号在60秒内再次发送验证码，验证码加上系统时间
        redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone, code, 10, TimeUnit.MINUTES);
        return R.ok();
    }

    /**
     * TODO 代码太丑
     * TODO 重定向携带数据，利用session原理。将数据放在session中。
     * 只要调到下一个页面取出这个数据以后，session里面的数据就会删掉
     * TODO 1、分布式下的session问题
     * RedirectAttributes：模拟重定向携带数据
     *
     * @param vo
     * @param result
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/regist")
    public String regist(@Valid UserRegistVo vo, BindingResult result, RedirectAttributes redirectAttributes) {
        Map<String, String> errors = new HashMap<>();
        // jsr303校验传输过来的数据是否合法
        if (result.hasErrors()) {
            errors = result.getFieldErrors().stream().collect(Collectors.
                    toMap(FieldError::getField, FieldError::getDefaultMessage));
            redirectAttributes.addFlashAttribute("errors", errors);
            // 用户注册-》/regist【post】--》转发/reg.html（路径映射都是get方式访问的）
            // 校验出错，转发到注册页
            return "redirect:http://auth.zohnmall.com/reg.html";
        }
        // 真正注册，调用远程服务进行注册
        // 1、校验验证码
        String code = vo.getCode();
        String s = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
        // 判断是否获取了验证码
        if (!StringUtils.isEmpty(s)) {
            // 判断验证码是否正确
            if (code.equals(s.split("_")[0])) {
                // 删除验证码;令牌机制
                redisTemplate.delete(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
                // 验证码通过
                // 调用远程服务进行注册
                R r = memberFeignService.regist(vo);
                if(r.getCode() == 0) {
                    // 成功

                    return "redirect:http://auth.zohnmall.com/login.html";
                } else {
                    // 失败
                    errors.put("msg", r.getData("msg", new TypeReference<String>(){}));
                }
            } else {
                errors.put("code", "验证码错误");
            }
        } else {
            errors.put("code", "验证码错误");
        }
        redirectAttributes.addFlashAttribute("errors", errors);
        return "redirect:http://auth.zohnmall.com/reg.html";
    }

    @GetMapping("/login.html")
    public String loginPage(HttpSession session) {
        Object attribute = session.getAttribute(AuthServerConstant.LOGIN_USER);
        if(attribute == null) {
            // 没登录
            return "login";
        } else {
        return "redirect:http://zohnmall.com";
        }
    }

    @PostMapping("/login")
    public String login(UserLoginVo vo, RedirectAttributes redirectAttributes, HttpSession session) {
        // 远程登录
        R login = memberFeignService.login(vo);
        if(login.getCode() == 0) {
            // 成功放到session中
            MemberRespVo data = login.getData("data", new TypeReference<MemberRespVo>() {});
            session.setAttribute(AuthServerConstant.LOGIN_USER, data);
            return "redirect:http://zohnmall.com";
        } else {
            Map<String, String> errors = new HashMap<>();
            errors.put("msg", login.getData("msg", new TypeReference<String>(){}));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:http://auth.zohnmall.com/login.html";
        }

    }
}
