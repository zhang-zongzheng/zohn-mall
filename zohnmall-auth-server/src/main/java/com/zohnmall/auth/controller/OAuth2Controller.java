package com.zohnmall.auth.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.zohnmall.auth.feign.MemberFeignService;
import com.zohnmall.common.vo.MemberRespVo;
import com.zohnmall.auth.vo.SocialUser;
import com.zohnmall.common.utils.HttpUtils;
import com.zohnmall.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 处理社交登录请求
 */
@Slf4j
@Controller
public class OAuth2Controller {
    @Autowired
    MemberFeignService memberFeignService;
    @Value("${gitee.client-id}")
    String clientId;
    @Value("${gitee.redirect-uri}")
    String redirectUri;
    @Value("${gitee.client-secret}")
    String clientSecret;

    /**
     * 社交登录成功回调
     * http://zohnmall.com/oauth2.0/gitee/success
     * @param code
     * @return
     * @throws Exception
     */
    @GetMapping("/oauth2.0/gitee/success")
    public String gitee(@RequestParam("code") String code, HttpSession session, HttpServletResponse servletResponse) throws Exception {
        HashMap<String, String> header = new HashMap<>();
        HashMap<String, String> query = new HashMap<>();
        HashMap<String, String> map = new HashMap<>();
        map.put("grant_type", "authorization_code");
        map.put("client_id", clientId);
        map.put("redirect_uri", redirectUri);
        map.put("client_secret", clientSecret);
        map.put("code", code);
        // 1、根据code换取access token
        HttpResponse response = HttpUtils.doPost("https://gitee.com", "/oauth/token", "post", header, query, map);

        if(response.getStatusLine().getStatusCode() == 200) {
            // 获取到了accesstoken；gitee获取用户id需要再发送一个请求https://gitee.com/api/v5/user
            String json = EntityUtils.toString(response.getEntity());
            SocialUser socialUser = JSON.parseObject(json, SocialUser.class);
            // 请求信息
            Map<String, String> userQuery = new HashMap<String, String>();
            userQuery.put("access_token", socialUser.getAccess_token());
            // 获取gitee获取授权用户的资料
            HttpResponse userResponse = HttpUtils.doGet("https://gitee.com", "/api/v5/user", "get", header, userQuery);
            if(userResponse.getStatusLine().getStatusCode() == 200) {
                // 获取用户id
                String userInfoJsonStr = EntityUtils.toString(userResponse.getEntity());
                JSONObject userInfoJson = JSONObject.parseObject(userInfoJsonStr);
                // gitee用户id
                String id = String.valueOf(userInfoJson.get("id"));
                // gitee用户名
                String name = String.valueOf(userInfoJson.get("name"));

                socialUser.setUid(id);
                socialUser.setName(name);

                System.out.println(socialUser);
                // 知道当前是哪个社交用户了
                // 1)、当前用户如果是第一次进网站，自动注册（为当前社交用户生成一个会员信息账号，以后这个社交账号就对应指定的会员）
                // 登录或者注册这个社交用户·
                R oauthLogin = memberFeignService.oauthLogin(socialUser);
                if(oauthLogin.getCode() == 0) {
                    MemberRespVo data = oauthLogin.getData("data", new TypeReference<MemberRespVo>() {});
                    log.info("登录成功：用户信息" + data.toString());
                    // 1、第一次使用session:命令浏览器保存session,JSESSIONID这个cookie
                    //  浏览器访问哪个网站就会带上哪个网站的cookie;
                    //  子域之间：   zohnmall.com auth.zohnmall.com  order.zohnmall.com
                    //  生成session时（指定域名为父域名），即使是子域系统的session也能让父域直接使用
                    //TODO 1、默认发的令牌，session=xxx，作用域：当前域； （解决子域session共享问题）
                    //TODO 2、使用JSON的序列化方式来序列化对象数据到redis中
                    session.setAttribute("loginUser", data);
                    // 2、登录成功就跳回首页
                    return "redirect:http://zohnmall.com";
                }
            }
        }
        return "redirect:http://auth.zohnmall.com/login.html";
    }
}
