package com.zohnmall.auth.vo;

import lombok.Data;

@Data
public class UserLoginVo {
    private String loginAcct;
    private String password;
}
