package com.zohnmall.common.constant;

public class ProductConstant {
    // 商品属性枚举
    public enum AttrEnum {
        ATTR_TYPE_BASE(1, "基本属性"), ATTR_TYPE_SALE(0, "销售属性");

        private int code;
        private String message;
        AttrEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    // 商品状态枚举
    public enum StatusEnum {
        NEW_SPU(0, "新建"), SPU_UP(1, "商品商家"),SPU_DOWN(2, "商品下架");

        private int code;
        private String message;
        StatusEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}
