package com.zohnmall.common.to.mq;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StockLockedTo implements Serializable {
    private Long id; // 库存工作单id
    private StockDetailTo detail; // 详情
}
