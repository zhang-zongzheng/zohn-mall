package com.zohnmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 1、如何使用Nacox作为配置中心统一管理配置
 *  1）、引入依赖 nacos-config
 *  2）、创建一个bootstrap.properties(spring.application.name=zohnmall-coupon
 * spring.cloud.nacos.config.server-addr=127.0.0.1:8848)
 *  3)、需要给配置中心默认添加一个叫 数据集（Data Id） zohnmall-coupon.properties （应用名.properties)
 *  4）、给应用名添加任何配置
 *  5）、@RefreshScope:动态刷新配置；@Value("${配置项的命}")：获取到配置
 *      如果配置中心和当前应用的配置文件中都配置了相同的项，优先使用配置中心的配置
 * 2、细节
 *  1）、命名空间：配置隔离：
 *      默认public(保留空间)；默认新增的所有配置都在public空间。
 *      1、开发，测试，生产环境:利空命名空间来做环境隔离
 *          注意：在bootstrap.properties：配置上，需要使用哪个命名空间下的配置（使用唯一ID）
 *          spring.cloud.nacos.config.namespace=9a8e8cfe-b2b5-47d6-a8c3-b36cb53f1a92
 *      2、每一个微服务之间互相隔离配置，每一个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置
 *  2）、配置集：所有配置的集合
 *
 *  3）、配置集Id：类似配置文件名
 *      Data ID：配置文件名
 *
 *  4）、配置分组
 *      默认所有的配置集都属于：DEFAULT_GROUP
 *      1111,618.1212
 *
 * 每个微服务创建自己的命名空间，使用配置分组区分环境，dev，test，prop环境
 *
 * 3、同时加载多个配置集
 *  1)、为服务器任何配置信息，任何配置文件都可以放在配置中心中
 *  2）、只需要在boostrap.properties说明加载配置中心哪位配置文件即可
 *  3）、@Value，@ConfigurationProperties
 */
@EnableFeignClients(basePackages = "com.zohnmall.coupon.feign")
@SpringBootApplication
@EnableDiscoveryClient
public class ZohnmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallCouponApplication.class, args);
    }

}
