package com.zohnmall.coupon.dao;

import com.zohnmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 03:52:23
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
