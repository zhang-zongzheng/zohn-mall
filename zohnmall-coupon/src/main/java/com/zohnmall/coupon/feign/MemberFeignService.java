package com.zohnmall.coupon.feign;

import com.zohnmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("zohnmall-member")
public interface MemberFeignService {
    @RequestMapping("/member/member/coupon/list")
    public R couponList();
}
