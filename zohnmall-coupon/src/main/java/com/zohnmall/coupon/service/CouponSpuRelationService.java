package com.zohnmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.coupon.entity.CouponSpuRelationEntity;
import com.zohnmall.common.utils.PageUtils;

import java.util.Map;

/**
 * 优惠券与产品关联
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 03:52:23
 */
public interface CouponSpuRelationService extends IService<CouponSpuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

