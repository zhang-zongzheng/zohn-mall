package com.zohnmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 03:52:23
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

