package com.zohnmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.to.SkuReductionTo;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 03:52:23
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTo skuReductionTo);
}

