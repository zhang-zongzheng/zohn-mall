package com.zohnmall.coupon.service.impl;

import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.common.utils.Query;
import com.zohnmall.coupon.dao.SkuLadderDao;
import com.zohnmall.coupon.entity.SkuLadderEntity;
import com.zohnmall.coupon.service.SkuLadderService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("skuLadderService")
public class SkuLadderServiceImpl extends ServiceImpl<SkuLadderDao, SkuLadderEntity> implements SkuLadderService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuLadderEntity> page = this.page(
                new Query<SkuLadderEntity>().getPage(params),
                new QueryWrapper<SkuLadderEntity>()
        );

        return new PageUtils(page);
    }

}