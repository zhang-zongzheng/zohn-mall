package com.zohnmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 1、开启服务注册发现
 *      (配置nacos注册中心地址）
 * 2、逻辑删除
 *  1)、配置全局的逻辑删除规则（可省略）
 *  2）、配置逻辑删除的组件Bean（高版本可省略）
 *  3）、给Bean加上逻辑删除注解 @TableLogic
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ZohnmallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallGatewayApplication.class, args);
    }

}
