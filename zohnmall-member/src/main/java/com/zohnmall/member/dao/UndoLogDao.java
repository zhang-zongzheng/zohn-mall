package com.zohnmall.member.dao;

import com.zohnmall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 03:58:17
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
