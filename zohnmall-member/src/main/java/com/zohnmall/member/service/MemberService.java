package com.zohnmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.member.entity.MemberEntity;
import com.zohnmall.member.exception.PhoneException;
import com.zohnmall.member.exception.UsernameException;
import com.zohnmall.member.vo.MemberLoginVo;
import com.zohnmall.member.vo.MemberRegistVo;
import com.zohnmall.member.vo.SocialUser;

import java.util.Map;

/**
 * 会员
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 03:58:17
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneException;

    void checkUsernameUnique(String username) throws UsernameException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity login(SocialUser socialUserGitee);
}

