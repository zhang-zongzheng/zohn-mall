package com.zohnmall.member;


import org.apache.commons.codec.digest.DigestUtils;


import org.apache.commons.codec.digest.Md5Crypt;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//@SpringBootTest
class ZohnmallMemberApplicationTests {

    @Test
    void contextLoads() {
//        String s = DigestUtils.md5Hex("123456");
//        System.out.println(s);
//
//        // 盐值加密;随机值
//        String s1 = Md5Crypt.md5Crypt("123456".getBytes(), "$1$zzzz");
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");

        boolean matches = bCryptPasswordEncoder.matches("123456", "$2a$10$zlCGkAUxIAvk7zaAYwIofOtZyRgBl2JWD0/HJemlo7tNCIc98HDEi");

        System.out.println(encode);
        System.out.println(matches);
    }

}
