package com.zohnmall.order;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * RabbitMQ
 * 1、引入amqp场景
 * 2、给容器中自动配置了 RabbitTemplate、AmqpAdmin
 * 3、配置文件中配置信息
 * 4、@EnableRabbit
 * 5、监听消息：使用RabbitListener；必须开启@EnableRabbit
 *      @RabbitListener:类+方法上（监听哪些队列0）
 *      @RabbitHandler:标在方法上（冲在区分不同的消息）
 *
 */
@EnableFeignClients
@EnableRedisHttpSession
@EnableDiscoveryClient
@EnableRabbit
@SpringBootApplication
public class ZohnmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallOrderApplication.class, args);
    }

}
