package com.zohnmall.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class MyRabbitConfig {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplate
     * 1、服务器收到消息就回调 ConfirmCallback
     *      1、# 开启发送端确认
     *      spring.rabbitmq.publisher-confirms=true
     *      2、设置确认回调
     * 2、消息正确抵达队列进行回调 ReturnCallback
     *      1、# 开启发送端确认returns
     *      spring.rabbitmq.publisher-returns=true
     *      # 只要抵达队列，以异步方式优先回调returnsConfirm
     *      spring.rabbitmq.template.mandatory=true
     *      2、设置确认回调
     * 3、消费端确认（保证每个消息被正确消费，此时才可以broker删除这个消息）
     *      spring.rabbitmq.listener.direct.acknowledge-mode=manual 手动签收
     *      1、默认是自动确认的，只要消息接收到，客户端会自动确认，服务端就会移除消息
     *          问题：
     *              收到很多消息，自动回复给服务器ack，只有一个消息处理成功后宕机了。出现消息丢失
     *              手动确认消息。只要没有明确告诉MQ已经消费了消息，没有Ack，消息就一直是unacked装填。及时Consumer宕机，
     *              消息不会丢失，会重新变为Ready状态，下一次有新的consumer进来就发给他
     *      2、如何签收
     *          channel.basicAck(deliveryTag, false); 签收:业务成功完成就应该签收
     *          channel.basicNack(deliveryTag, false, true); 拒签：业务失败，拒签
     */
    @PostConstruct //MyRabbitConfig对象创建完成以后，执行这个方法
    public void initRabbitTemplate() {
        // 设置确认回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             * 服务器Broker收到
             * 1、只要消息抵达服务器Broker就ack=true,
             * 防止消息丢失问题：
             * 1、做好消息确认机制（publisher,consumer【手动ack】）
             * 2、每一个发送的消息都在数据库做好记录。定期将失败的消息再次发送一遍
             *
             * @param correlationData 当前消息的唯一关联数据（消息的唯一id）
             * @param b 消息是否成功收到
             * @param s 失败的原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean b, String s) {
                // TODO 可靠消息；服务器收到了，修改消息的状态。
//                System.out.println("confirm...correlationData[" + correlationData + "]==>ack[" + b +"]==>s["+s+"]");
            }
        });

        // 设置消息抵达队列的确认回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             * 只要消息没有投递给指定的队列，就触发这个失败回调
             * @param message  投递失败的消息详情
             * @param i     回复的状态码
             * @param s     回复的文本内容
             * @param s1    消息发给哪个交换机
             * @param s2    消息用哪个路由键
             */
            @Override
            public void returnedMessage(Message message, int i, String s, String s1, String s2) {
                // 报错误了，修改数据库当前消息的错误状态-》错误
//                System.out.println("Fail Message["+ message +"]==>" + i + "==>" + s +"==>" + s1 + "==>" + s2);
            }
        });
    }
}
