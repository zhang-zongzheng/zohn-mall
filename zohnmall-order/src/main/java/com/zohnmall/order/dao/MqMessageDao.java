package com.zohnmall.order.dao;

import com.zohnmall.order.entity.MqMessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 04:06:23
 */
@Mapper
public interface MqMessageDao extends BaseMapper<MqMessageEntity> {
	
}
