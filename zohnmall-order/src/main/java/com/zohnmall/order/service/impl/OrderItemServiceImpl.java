package com.zohnmall.order.service.impl;

import com.rabbitmq.client.Channel;
import com.zohnmall.order.entity.OrderEntity;
import com.zohnmall.order.entity.OrderReturnReasonEntity;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.common.utils.Query;

import com.zohnmall.order.dao.OrderItemDao;
import com.zohnmall.order.entity.OrderItemEntity;
import com.zohnmall.order.service.OrderItemService;

@RabbitListener(queues = {"hello-java-queue"})
@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * queues:声明需要监听的所有队列
     *
     * 参数可以写以下类型
     * 1、org.springframework.amqp.core.Message ：原生消息详细信息，头+体；
     * 2、T<发送的消息的类型> OrderReturnReasonEntity； Spring自动转换
     * 3、Channel ; 当前传输数据的通道
     *
     * Queue：可以有很多人监听。只要收到消息，队列就会删除消息，而且只能有一个收到此消息
     * 场景：
     *      1）、订单服务启动多个，同一个消息，只能有一个客户端收到
     *      2）、只有一个消息完全处理完，方法运行结束，才可以接收到下一个消息
     */
//    @RabbitListener(queues = {"hello-java-queue"})
    @RabbitHandler
    public void receiveMessage(Message message,
                               OrderReturnReasonEntity content,
                               Channel channel) throws InterruptedException, IOException {
        // Body:'{"id":1,"name":"hahaha","sort":null,"status":null,"createTime":1642237308119}
        System.out.println("接收到消息……：" + content);
        byte[] body = message.getBody();
        //消息头属性信息
        MessageProperties messageProperties = message.getMessageProperties();
//        Thread.sleep(3000);
        System.out.println("消息处理完成=>" + content.getName());
        // channel内按顺序自增的
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        System.out.println("deliveryTag：" + deliveryTag);
        // 确认消费，非批量模式
        try {
            if(deliveryTag % 2 == 0) {
                // 收货
                channel.basicAck(deliveryTag, false);
                System.out.println("签收了货物……"  + deliveryTag);
            } else {
                // 退货
                channel.basicNack(deliveryTag, false, false);
//                channel.basicReject();
                System.out.println("没有签收货物……"  + deliveryTag);
            }

        } catch (Exception e) {
            // 网络终端
        }

    }
    @RabbitHandler
    public void receiveMessage(OrderEntity content) throws InterruptedException {
        System.out.println("消息处理完成=>" + content);

    }
}