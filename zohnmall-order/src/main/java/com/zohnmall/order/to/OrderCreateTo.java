package com.zohnmall.order.to;

import com.zohnmall.order.entity.OrderEntity;
import com.zohnmall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderCreateTo {
    private OrderEntity order;
    private List<OrderItemEntity> items;
    private BigDecimal peyPrice; // 订单计算出的应付总价格
    private BigDecimal fare; // 运费
}
