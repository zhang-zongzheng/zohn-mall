package com.zohnmall.order.vo;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 封装订单提交的数据
 */
@ToString
@Data
public class OrderSubmitVo {
    private Long addrId; //收货地址的id
    private Integer payType; // 支付方式
    // 无需提交需要购买的商品，而是去购物车再获取
    // 优惠，发票。。。。
    private String orderToken; // 防重令牌
    private BigDecimal payPrice; // 应付价格 验价
    private String note; // 订单备注
    // 用户相关信息都在session取出即可
}
