package com.zohnmall.order.vo;

import com.zohnmall.order.entity.OrderEntity;
import lombok.Data;

@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;
    private Integer code; // 0就是成功；否则错误。错误状态码
}
