package com.zohnmall.order.web;

import com.alipay.api.AlipayApiException;
import com.zohnmall.order.config.AlipayTemplate;
import com.zohnmall.order.service.OrderService;
import com.zohnmall.order.vo.PayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PayWebController {
    @Autowired
    AlipayTemplate alipayTemplate;
    @Autowired
    OrderService orderService;

    /**
     * 1、支付成功后将支付页让浏览器展示
     * 2、支付成功以后，跳到用户的订单列表页
     * @param orderSn
     * @return
     * @throws AlipayApiException
     */
    @ResponseBody
    @GetMapping(value = "/aliPayOrder", produces = "text/html")
    public String payOrder(@RequestParam("orderSn") String orderSn) throws AlipayApiException {

        PayVo payVo = orderService.getOrderPay(orderSn);
        // 返回的是一个页面，将此页面直接交给浏览器
        String pay = alipayTemplate.pay(payVo);
        return pay;
    }

}
