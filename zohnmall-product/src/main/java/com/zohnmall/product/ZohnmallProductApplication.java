package com.zohnmall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/*
* 1、整合MyBatis-Plus
*   1)、导入依赖
*   2）、配置
*       1、配置数据源
*           1）、导入数据库驱动
*           2)、配置数据源
*       2、配置MyBatis-Plus
*           1)、使用@MapperScan
*           2)、sql映射文件
* 3、JSR303
*   1）、给Bean添加校验注解:javax.validation.constraints，并定义自己的message提示
*   2）、开启校验功能@Valid
*       效果：校验错误后会有默认的响应
*   3）、给校验的bean后紧跟一个BindingResult，就可以获取到校验的结果
*   4）、分组校验
*       1)、 @NotNull(message = "修改必须指定品牌id", groups = {UpdateGroup.class})
*           给校验注解标注什么情况需要进行校验
*       2）、@Validated(AddGroup.class)
*
* 4、 统一的异常处理（完成多场景复杂校验）
*   @ControllerAdvice
*   1）、编写异常处理类，使用@ControllerAdvice，使用@ExceptionHandler标注方法可以处理异常，
*   2）、@Validated({AddGroup.class})
*   3）、默认没有指定分组的校验注解，在分组校验的情况下不生效，只会在@Validated生效；
*
* 5、 自定义校验
*   1）、编写一个自定义校验注解
*   2）、编写一个自定义的校验器
*   3）、关联自定义的校验器和自定义的校验注解
*
* 6、 整合redis
*   1)/引入data-redis-starter
*   2）、简单配置redis的Host等信息
*   3）、使用springboot自动配置好饿StringRedisTemplate来操作redis
*
* 7、整合redisson作为分布式锁的功能框架
*   1）、引入依赖<!-- https://mvnrepository.com/artifact/org.redisson/redisson -->
<dependency>
    <groupId>org.redisson</groupId>
    <artifactId>redisson</artifactId>
    <version>3.12.0</version>
</dependency>
* 8、整合SpringCache简化缓存开发
*       1）、引入依赖spring-boot-starter-cache、spring-boot-starter-data-redis
*       2）、写配置
*           （1）、自动配置了哪些
*               CacheAutoConfiguration会导入RedisCacheConfiguration;
*               自动配好了缓存管理器RedisCacheManager
*           （2）、配置使用redis作为缓存
*       3）、测试使用缓存
*       spring.cache.type=redis
@Cacheable: Triggers cache population. 触发将数据保存到缓存的操作
@CacheEvict: Triggers cache eviction.   触发将数据从缓存删除的操作
@CachePut: Updates the cache without interfering with the method execution.：不影响方法执行地更新缓存
@Caching: Regroups multiple cache operations to be applied on a method. 组合以上多个组合操作
@CacheConfig: Shares some common cache-related settings at class-level. 在类级别，共享缓存的相同配置
*       1）、开启缓存功能@EnableCaching
*       2）、只要使用注解就能完成缓存操作
*      4）、原理
*           CacheAutoConfiguration -》 RedisCacheConfiguration =》
*           自动配置了RedisCacheManager =》 初始化所有的缓存 =》 每个缓存决定使用什么配置
*           =》 如果RedisCacheConfiguration有就用已有的，没有就用默认配置
*           =》想改缓存的配置，只需要给容器中放一个RedisCacheConfiguration即可
*           =》就会应用到当前RedisCacheManager管理的所有缓存分区中
*
*
* */

@EnableRedisHttpSession
@EnableFeignClients(basePackages = "com.zohnmall.product.feign")
@EnableDiscoveryClient
@MapperScan("com.zohnmall.product.dao")
@SpringBootApplication
public class ZohnmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallProductApplication.class, args);
    }

}
