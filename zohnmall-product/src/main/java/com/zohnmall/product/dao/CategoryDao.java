package com.zohnmall.product.dao;

import com.zohnmall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 02:54:29
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	@Delete("DELETE FROM pms_category")
    public void deleteAll();

	@Insert("INSERT INTO pms_category SELECT * FROM pms_category_default")
    public void addDefault();
}
