package com.zohnmall.product.dao;

import com.zohnmall.product.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 02:54:29
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
