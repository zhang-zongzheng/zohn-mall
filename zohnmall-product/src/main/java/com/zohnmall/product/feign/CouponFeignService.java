package com.zohnmall.product.feign;

import com.zohnmall.common.to.SkuReductionTo;
import com.zohnmall.common.to.SpuBoundTo;
import com.zohnmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("zohnmall-coupon")
public interface CouponFeignService {
    /**
     * 1、CouponFeignService.saveSpuBonds(SpuBoundTo)
     *      1)、@RequestBody将这个对象转为json.
     *      2）、去注册中心找到服务zohnmall-coupon，给coupon/spubounds/save发送请求。将上一步转的json放在请求体位置，发送请求
     *      3）、对方服务收到请求。请求体里有json数据。
     *          (@RequestBody SpuBoundsEntity spuBounds)：将请求体的json转为SpuBoundsEntity
     * 只要json数据模型是兼容的，双方服务无需使用同一个TO
     * @param spuBoundTo
     * @return
     */
    @PostMapping("/coupon/spubounds/save")
    R saveSpuBonds(@RequestBody  SpuBoundTo spuBoundTo);

    @PostMapping("/coupon/skufullreduction/saveinfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
