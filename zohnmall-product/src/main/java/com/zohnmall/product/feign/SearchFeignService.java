package com.zohnmall.product.feign;

import com.zohnmall.common.to.es.SkuEsModel;
import com.zohnmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("zohnmall-search")
public interface SearchFeignService {
    // 上架商品
    @PostMapping("/search/save/product")
    public R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels);
}
