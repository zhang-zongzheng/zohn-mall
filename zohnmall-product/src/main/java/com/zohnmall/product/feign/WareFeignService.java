package com.zohnmall.product.feign;

import com.zohnmall.common.to.SkuHasStockVo;
import com.zohnmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("zohnmall-ware")
public interface WareFeignService {

    /**
     * 1、R设计的时候可以加上反省
     * 2、直接返回我们想要的结果
     * 3、自己封装解析结果
     * @param skuIds
     * @return
     */

    // 查询sku是否有库存
    @PostMapping("/ware/waresku/hasstock")
    public R getSkusHasStock(@RequestBody List<Long> skuIds);
}
