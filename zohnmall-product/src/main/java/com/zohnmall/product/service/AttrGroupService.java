package com.zohnmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.product.entity.AttrGroupEntity;
import com.zohnmall.product.vo.AttrGroupWithAttrsVo;
import com.zohnmall.product.vo.SkuItemVo;
import com.zohnmall.product.vo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 02:54:29
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId);

    List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId);
}

