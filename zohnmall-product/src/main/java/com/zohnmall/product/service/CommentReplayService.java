package com.zohnmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 02:54:29
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

