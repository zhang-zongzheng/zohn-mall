package com.zohnmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.product.entity.SpuCommentEntity;
import com.zohnmall.common.utils.PageUtils;

import java.util.Map;

/**
 * 商品评价
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 02:54:29
 */
public interface SpuCommentService extends IService<SpuCommentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

