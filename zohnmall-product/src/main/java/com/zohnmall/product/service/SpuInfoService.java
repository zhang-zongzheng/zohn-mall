package com.zohnmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.product.entity.SpuInfoDescEntity;
import com.zohnmall.product.entity.SpuInfoEntity;
import com.zohnmall.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 02:54:29
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo vo);

    void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity);

    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 商品上架
     * @param spuId
     */
    void up(Long spuId);

    SpuInfoEntity getSpuInfoBySkuId(Long skuId);
}

