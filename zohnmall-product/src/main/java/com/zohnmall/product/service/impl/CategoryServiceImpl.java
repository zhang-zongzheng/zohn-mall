package com.zohnmall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zohnmall.product.service.CategoryBrandRelationService;
import com.zohnmall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.common.utils.Query;

import com.zohnmall.product.dao.CategoryDao;
import com.zohnmall.product.entity.CategoryEntity;
import com.zohnmall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    CategoryDao categoryDao;
    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    RedissonClient redisson;

//    private Map<String, Object> cache = new HashMap<>();

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    // 自定义
    @Override
    public List<CategoryEntity> listWithTree2() {
        // 1、查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
        // 自定义
        int len = entities.size();
        if (len == 0) {
            return null;
        }
        CategoryEntity temp;
        int index = 0;
        Long parentCid;
        ArrayList<CategoryEntity> root = new ArrayList<CategoryEntity>();
        Map<Long, ArrayList<CategoryEntity>> level1 = new HashMap<Long, ArrayList<CategoryEntity>>();
        for (CategoryEntity entity : entities) {
            parentCid = entity.getParentCid();
            // 如果有父类，则存到map<父类ID, List<>>
            if (parentCid != 0) {
                if (!level1.containsKey(parentCid)) {
                    ArrayList<CategoryEntity> list = new ArrayList<CategoryEntity>();
                    list.add(entity);
                    level1.put(parentCid, list);
                } else {
                    level1.get(parentCid).add(entity);
                }
            } else {
                root.add(entity);
            }
        }
        for (CategoryEntity entity : entities) {
            if (level1.containsKey(entity.getCatId())) {
                entity.setChildren(level1.get(entity.getCatId()));
            }
        }
        long endTime = System.currentTimeMillis();    //获取结束时间
        return root;
    }


    @Override
    public List<CategoryEntity> listWithTree() {
        // 1、查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
//        long startTime = System.currentTimeMillis();
        // 2、组装成父子的树形结构

        // 2.1)、找到所有的一级分类
        List<CategoryEntity> level1Menus = entities.stream().filter(categoryEntity ->
                        categoryEntity.getParentCid() == 0
                ).map((menu) -> {
                    menu.setChildren(getChildrens(menu, entities));
                    return menu;
                })
                .sorted((menu1, menu2) -> {
                    return menu1.getSort() - menu2.getSort();
                })
                .collect(Collectors.toList());

        long endTime = System.currentTimeMillis();    //获取结束时间

//        System.out.println("tree1运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间
        return level1Menus;
    }

    // 递归查找所有菜单的子菜单
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
                    return categoryEntity.getParentCid() == root.getCatId();
                }).map(categoryEntity -> {
                    // 1、找到子菜单
                    categoryEntity.setChildren(getChildrens(categoryEntity, all));
                    return categoryEntity;
                })
                .sorted((menu1, menu2) -> {
                    // 2、菜单的排序
                    return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
                })
                .collect(Collectors.toList());
        return children;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        //TODO 1、检查当前删除的菜单，是否被别的地方引用

        //逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    // 恢复默认数据
    @Transactional
    @Override
    public void defaultDta() {
        categoryDao.deleteAll();
        categoryDao.addDefault();
    }

    // [2, 25, 225]
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId, paths);
        Collections.reverse(parentPath);
        return parentPath.toArray(new Long[parentPath.size()]);
    }

    /**
     * 级联更新所有关联的数据
     * @CacheEvict 失效模式
     * 1、同时进行多钟缓存操作 @Caching
     * 2、指定删除某个分区下的所有数据  @CacheEvict(value = {"category"}, allEntries = true)
     * 3、存储同一类型的数据，都可以指定成同一个分区。分区明默认就是缓存前缀
     * @param category
     */
//    @Caching(evict = {
//            @CacheEvict(value = {"category"}, key = "'getLevel1Categorys'"),
//            @CacheEvict(value = {"category"}, key = "'getCatalogJson'")
//    })
    @CacheEvict(value = {"category"}, allEntries = true) // 失效模式
//    @CachePut // 双写模式
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        // 双写模式：更新完后同时修改缓存中的数据
        // 失效模式：直接删掉缓存数据redis.del("catalogJSON")；等待下次主动查询进行更新
    }

    /**
     * // 1、每一个需要缓存的数据都来指定要放到哪个名字的缓存，【缓存的分区(按照业务类型分类)】
     * // 2、@Cacheable({"category"})
     * //      代表当前方法的结果需要缓存，如果缓存中有，方法不用调用。
     * //      如果缓存中没有，会调用方法，最后将方法的结果放入缓存
     * // 3、默认行为
     * //      1）、如果缓存中有，方法不用调用
     * //      2）、key默认自动生成：缓存的名字： category::SimpleKey[](自主生成的key值)
     * //      3）、缓存的value的值，默认使用jdk序列化机制，将序列化后的数据存到redis
     * //      4）、默认ttl时间：-1
     * 自定义操作
     * 1）、指定生成的缓存使用的key：   key属性指定，接收一个SpEL
     * 2）、指定缓存数据的存活时间:     配置文件中修改ttl
     * 3）、将数据保存为json格式：
     *
     * 4、spring-cache不足：
     *      1）、读模式：
     *          缓存穿透：查询一个null数据。解决：缓存空数据 spring.cache.redis.cache-null-values=true
     *          缓存击穿：大量并发进来同时查询一个正好过期的数据。 解决：加锁;? 默认是无加锁的;sync = true(加锁，解决击穿)
     *          缓存雪崩： 大量的key同时过期。 解决：加随机时间。加上过期时间。spring.cache.redis.time-to-live=3600000
     *      2）、写模式：（缓存与数据库一致）
     *          1）、读写加锁。
     *          2）、引入Canal,感知到MySQL的更新去更新数据库
     *          3）、读多写多，直接去数据库查询就行
     *     总结：
     *        常规数据：（读多写少，即时性，一致性要求不高的数据）：完全可以使用spring-cache；写模式（只要缓存的数据有过期时间就足够了）
     *        特殊数据：特殊设计
     *   原理：
     *      CacheManager(RedisCacheManager)->Cache(RedisCache)->Cache负责缓存的读写
     * @return
     */
    @Cacheable(value = {"category"}, key = "#root.method.name", sync = true)
    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        System.out.println("getLevel1Categorys");
        List<CategoryEntity> categoryEntities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        return categoryEntities;
    }

    @Cacheable(value = {"category"}, key = "#root.methodName")
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        System.out.println("查询了数据库……" + Thread.currentThread());
        List<CategoryEntity> categoryEntities1 = baseMapper.selectList(null);
        // 1。查出所有1级分类
        List<CategoryEntity> level1Categorys = getParent_cid(categoryEntities1, 0L);
        // 2、封装数据
        Map<String, List<Catelog2Vo>> map = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            // 1、每一个的一级分类,查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(categoryEntities1, v.getCatId());
            // 2、封装上面的结果
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    // 1、找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParent_cid(categoryEntities1, l2.getCatId());
                    if (level3Catelog != null) {
                        List<Catelog2Vo.Catelog3Vo> collect = level3Catelog.stream().map(l3 -> {
                            // 2、封装成指定格式
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(collect);

                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2Vos;
        }));
        return map;
    }
    //TODO 产生堆外内存溢出：OutOfDirectMemoryError
    //1）、springboot2.0以后默认使用lettuce作为操作redis的客户端。使用netty进行网络通信
    //2）、lettuce的bug导致堆外内存溢出 -Xmx300m：netty如果没有指定堆外内存，默认使用-Xmx300m
    //  可以通过-Dio.netty.maxDirectMemory进行设置
    // 解决方案：不能使用-Dio.netty.maxDirectMemory只去调大堆外内存
    //1）、升级lettuce客户端。  2）、切换使用jedis
    // lettuce、jedis操作redis的底层客户端。spring再次封装redisTemplate

    //    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson2() {
        // 给缓存中放json字符串,拿出的json字符串，还要逆转为能用的对象类型：【序列化和反序列化】
        /**
         * 1、空结果缓存：解决缓存穿透
         * 2、设置过期时间（加随机值）：解决缓存雪崩
         * 3、加速：解决缓存击穿
         */
        // 1、加入缓存逻辑,缓存中存的数据是json字符串（复杂对象）。
        // JSON跨语言跨平台的兼容
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            // 2、缓存中没有，查询数据库
            System.out.println("缓存不命中……将要查询数据库……");
            Map<String, List<Catelog2Vo>> catalogJsonFromDb = getCatalogJsonFromDbWithRedissonLock();
            return catalogJsonFromDb;
        }
        System.out.println("缓存命中……直接返回……");
        // 转为我们指定的对象
        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });
        return result;
    }

    /**
     * 缓存里面的数据如何和数据库保持一致
     * 缓存数据一致性问题
     * 1）、双写模式
     * 2）、失效模式
     *
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedissonLock() {
        // 1、锁的名字。锁的粒度越细，运行越快；
        // 锁的粒度:具体缓存的是某个数据，11-号商品加锁：product-11-lock
        RLock lock = redisson.getLock("catalogJson-lock");
        lock.lock();
        Map<String, List<Catelog2Vo>> dataFromDb;
        try {
            dataFromDb = getDataFromDb();
        } finally {
            lock.unlock();
        }
        return dataFromDb;
    }

    // 从数据库查询并封装分类数据
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedisLock() {
        // 1、占分布式锁。去redis占坑
        String uuid = UUID.randomUUID().toString();
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
        if (lock) {
            System.out.println("获取分布式锁成功……");
            // 加锁成功...执行业务
            // 2、设置过期时间,必须和加锁是同步的，原子的
//            redisTemplate.expire("lock", 30, TimeUnit.SECONDS);
            Map<String, List<Catelog2Vo>> dataFromDb;
            try {
                dataFromDb = getDataFromDb();
            } finally {
                String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                // 删除锁
                Long lock1 = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class),
                        Arrays.asList("lock"), uuid);
            }
            // 获取值对比，对比成功删除 = 原子操作 lua脚本解锁
//            String lockValue = redisTemplate.opsForValue().get("lock");
//            if(uuid.equals(lockValue)) {
//                // 删除自己的锁
//                redisTemplate.delete("lock"); // 删除锁
//            }
            return dataFromDb;
        } else {
            // 加锁失败...重试
            // 休眠100ms重试
            System.out.println("获取分布式锁失败……等待重试");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getCatalogJsonFromDbWithRedisLock(); // 自旋的方式
        }
    }

    private Map<String, List<Catelog2Vo>> getDataFromDb() {
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (!StringUtils.isEmpty(catalogJSON)) {
            // 缓存不为空，直接返回
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });
            return result;
        }

        System.out.println("查询了数据库……" + Thread.currentThread());

        List<CategoryEntity> categoryEntities1 = baseMapper.selectList(null);
        // 1。查出所有1级分类
        List<CategoryEntity> level1Categorys = getParent_cid(categoryEntities1, 0L);

        // 2、封装数据
        Map<String, List<Catelog2Vo>> map = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            // 1、每一个的一级分类,查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(categoryEntities1, v.getCatId());
            // 2、封装上面的结果
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    // 1、找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParent_cid(categoryEntities1, l2.getCatId());
                    if (level3Catelog != null) {
                        List<Catelog2Vo.Catelog3Vo> collect = level3Catelog.stream().map(l3 -> {
                            // 2、封装成指定格式
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(collect);

                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2Vos;
        }));
        // 3、查到的数据再放入缓存,将对象转为json放在缓存中
        String s = JSON.toJSONString(map);
        redisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);
        return map;
    }

    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithLocalLock() {

//        // 1、如果缓存中有就用缓存的
//        Map<String, List<Catelog2Vo>> catalogJson = (Map<String, List<Catelog2Vo>>) cache.get("catalogJson");
//
//        if(catalogJson == null) {
//        }
        // 只要是同一把锁,就能锁住，需要这个锁的所有线程
        // 1、synchronized (this):SpringBoot所有的组件在容器中都是单例模式，
        // TODO 本地锁： synchronized, JUC(Lock),在分布式情况下，想要锁住所有，必须使用分布式锁
        synchronized (this) {
            //得到锁以后，我们应该再去缓存中确定一次，如果没有才需要继续查询
            /**
             * 1、将数据库的多次查询变为一次
             */
            return getDataFromDb();
//        return catalogJson;
        }
    }

    private List<CategoryEntity> getParent_cid(List<CategoryEntity> categoryEntities1, Long parent_cid) {
        List<CategoryEntity> collect = categoryEntities1.stream().filter(item -> item.getParentCid() == parent_cid).collect(Collectors.toList());
//        return baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
        return collect;
    }

    private List<Long> findParentPath(Long catelogId, List<Long> paths) {
        // 1、收集当前节点ID
        paths.add(catelogId);
        CategoryEntity byId = this.getById(catelogId);
        if (byId.getParentCid() != 0) {
            findParentPath(byId.getParentCid(), paths);
        }
        return paths;

    }
}