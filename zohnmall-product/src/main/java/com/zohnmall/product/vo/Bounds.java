/**
  * Copyright 2021 bejson.com 
  */
package com.zohnmall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2021-12-10 14:41:46
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;


}