package com.zohnmall.search.controller;

import com.zohnmall.search.service.MallSearchService;
import com.zohnmall.search.vo.SearchParam;
import com.zohnmall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 商城检索-检索条件分析
 * 1、全文检索：skuTitle => keyword
 * 2、排序：saleCount(销量)、hotScore(热度分)、skuPrice(价格)
 * 3、hasStock、skuPrice区间、brandId、catalogId、attrs
 */
@Controller
public class SearchController {

    @Autowired
    MallSearchService mallSearchService;

    /**
     * 自动将页面提交过来的所有请求查询参数封装成指定的对象
     * @param param
     * @return
     */
    @GetMapping("/list.html")
    public String listPage(SearchParam param, Model model, HttpServletRequest request) {
        param.set_queryString(request.getQueryString());
        // 1、根据传递来的页面的查询参数，去es中检索商品
        SearchResult result = mallSearchService.search(param);
        model.addAttribute("result", result);
        model.addAttribute("keyword", result);
        return "list";
    }
}
