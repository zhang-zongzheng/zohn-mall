package com.zohnmall.search.service;

import com.zohnmall.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

public interface ProductSaveService {
    public boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
