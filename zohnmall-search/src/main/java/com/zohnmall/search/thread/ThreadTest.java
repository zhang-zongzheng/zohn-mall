package com.zohnmall.search.thread;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

public class ThreadTest {
    public static ExecutorService executor = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main....start...");

//        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果：" + i);
//        }, executor);

        /**
         * whenComplete、exceptionally方法完成后的感知
         */
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 0;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executor).whenComplete((res, exception) ->{
//            // 虽然能得到异常信息……但是没法修改返回数据。
//            System.out.println("异步任务成功完成了……结果是：" + res + ";异常是：" + exception);
//        }).exceptionally(throwable -> {
//            // 可以感知异常同时返回默认值
//            return 10;
//        });

        /**
         * handle方法执行完成后的处理
         */
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executor).handle((res, thr)->{
//            if(res != null) {
//                return res * 2;
//            }
//            if(thr != null) {
//                return 0;
//            }
//            return 0;
//        });

        /**
         * 线程串行化
         * 1）、thenRun,不能获取到上一步的执行结果，无返回值
         * , executor).thenRunAsync(() -> {
         *             System.out.println("任务二启动了……");
         *         }, executor);
         * 2）、thenAccept,能获取上一步执行结果，无返回值
         * 3)、thenApply， 能接收上一步执行结果，有返回值;
         * future.get()是阻塞方法
         */
        // thenRun
//        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executor).thenRunAsync(() -> {
//
//        }, executor);

        // thenAcceptAsync
//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executor).thenAcceptAsync(res -> {
//            System.out.println("任务二启动了……" + res);
//        }, executor);

        // thenApply
//        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executor).thenApplyAsync(res -> {
//            System.out.println("任务二启动了……" + res);
//            return "Hello" + res;
//        }, executor);



        /**
         * 两个都完成
         */
//        CompletableFuture<Object> future01 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("任务1结果：" + i);
//            return i;
//        }, executor);
//
//        CompletableFuture<Object> future02 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务2线程：" + Thread.currentThread().getId());
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println("任务2结果：");
//            return "Hello";
//        }, executor);

//        // runAfterBothAsync不能获取到返回值
//        future01.runAfterBothAsync(future02, ()->{
//            System.out.println("任务3开始");
//            System.out.println();
//        }, executor);

        // thenAcceptBothAsync可以获取返回值
//        future01.thenAcceptBothAsync(future02, (f1, f2) -> {
//            System.out.println("任务3开始……之前的结果:" + f1 + "-->" + f2);
//        },executor);

        // thenCombineAsync可以获取前面的返回值，也可以返回值
//        CompletableFuture<String> future = future01.thenCombineAsync(future02, (f1, f2) -> {
//            return f1 + ":" + f2 + " -> Haha";
//        }, executor);

        /**
         * 两个任务，只要有一个完成，就执行任务3
         * runAfterEitherAsync:不感知结果（不接受返回值），自己也无返回值
         * acceptEitherAsync:只接受返回值，自己不返回值
         * applyToEitherAsync:接收返回值，自己也返回值
         */
//        future01.runAfterEitherAsync(future02 ,()->{
//            System.out.println("任务3开始……之前的结果");
//        }, executor);

        //acceptEitherAsync
//        future01.acceptEitherAsync(future02, (res)->{
//            System.out.println("任务3开始……之前的结果" + res);
//        }, executor);

        // applyToEitherAsync
//        CompletableFuture<String> future = future01.applyToEitherAsync(future02, (res) -> {
//            System.out.println("任务3开始……之前的结果" + res);
//            return res.toString() + "-> 哈哈";
//        }, executor);
//        System.out.println("main...end :" + future.get());

        CompletableFuture<String> futureImg = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的图片信息");
            return "hello.jpg";
        }, executor);

        CompletableFuture<String> futureAttr = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的属性");
            return "黑色+265G";
        }, executor);

        CompletableFuture<String> futureDesc = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(3000);
                System.out.println("查询商品的介绍");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "华为";
        }, executor);

//        futureImg.get();futureAttr.get();futureDesc.get();

//        CompletableFuture<Void> allOf = CompletableFuture.allOf(futureImg, futureAttr, futureDesc);
//        allOf.get();    // 等待所有结果完成
        CompletableFuture<Object> anyOf = CompletableFuture.anyOf(futureImg, futureAttr, futureDesc);
        anyOf.get();

//        System.out.println("main....end..." + futureImg.get() + "=>" + futureAttr.get() + "=>" + futureDesc.get());
        System.out.println("main....end..." + anyOf.get());
        Queue<Integer> queue = new LinkedList<>();
    }

    public void thread(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main....start...");
        /**
         * 1 Thread
         * 2 Runnable
         * 3 Callable
         * 4 线程池 [ExecutorService] 给线程池直接提交任务 executorService.execute(new Runnable01());
         *    1、创建：
         *      1）、Executors
         *      2）、new ThreadPoolExecutor
         * 以后在业务代码中，上面三种启动线程的方式都不用。【将所有的多线程异步任务都交给线程池执行】
         *
         *      Future:可以获取到一部结果
         * 区别： 1和2不能得到返回值，3可以获取返回值
         *       1 2 3不能控制资源
         *       4可以控制资源，性能是稳定的
         */
//        Thread01 thread = new Thread01();
//        thread.start(); // 启动线程

//        Runnable01 runnable01 = new Runnable01();
//        new Thread(runnable01).start();
//

//        FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
//        new Thread(futureTask).start();
//        // 阻塞等待、等待整个线程执行完成获取返回结果
//        Integer integer = futureTask.get();
//        System.out.println(integer);

        // 当前系统中线程池只有一两个，每个异步任务，提交给线程池让他自己执行
        /**
         * ThreadPoolExecutor 七大参数
         * corePoolSize：[5]核心线程数一直存在[除非设置了allowCoreThreadTimeOut]； 线程池创建好后就准备就绪的线程数量，等待接收业务任务去执行
         *              5个  Thread thread = new Thread(); thread.start();
         * maximumPoolSize：最大线程数量； 控制资源
         * keepAliveTime： 存活时间。如果当前正在运行的线程数量大约核心线程数(maximumPoolSize - corePoolSize)，释放空闲线程。只要线程空闲大于指定的存活时间keepAliveTime
         * unit： 时间单位
         * BlockingQueue<Runnable> workQueue： 阻塞队列。如果任务有很多，就会将目前多地任务放在队列中，只要有空闲的线程，就回去队列里面去除新的任务继续执行
         * threadFactory：线程的创建工厂。
         * RejectedExecutionHandler handler：如果workQueue队列满了，按照我们指定的拒绝策略拒绝执行任务
         *
         * 工作顺序：
         *     1）、线程池创建，转备好core数量的核心现场，准备接受任务
         *     1.1、core满了，就将再进来的任务放入阻塞队列中。空闲的core就会自己去阻塞队列获取任务执行
         *     1.2、阻塞队列满了，就直接开新线程执行，最大只能开到max指定的数量
         *     1.3、若max满了就用拒绝策略RejectedExecutionHandler拒绝任务
         *     1.4、若max都执行完成，有很多空闲，在指定的时间以后keepAliveTime以后，释放max-core这些线程
         *          new LinkedBlockingDeque<>()；默认是Integer的最大值。内存可能不够
         *
         *     问题：一个线程池 core 7; max 20; queue 50, 100并发进来怎么分配？
         *     core 7立即执行 ， queue 50个进入队列，再开max 20 - core 7 = 13个进行执行，剩下的30个使用拒绝策略。
         *     如果不香抛弃还要执行，CallerRunsPolicy
         */
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
//        Executors.newCachedThreadPool(); 缓存，核心是0，所有都可以回收
//        Executors.newFixedThreadPool(); 固定大小，核心是最大，都不可回收
//        Executors.newScheduledThreadPool(); // 做定时任务的线程池
        Executors.newSingleThreadExecutor(); // 单线程的线程池，后台从队列里面获取任务，挨个执行
        System.out.println("main....end...");
    }

    public static class Thread01 extends Thread {
        @Override
        public void run() {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }
    }

    public static class Runnable01 implements Runnable {
        @Override
        public void run() {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }
    }

    public static class Callable01 implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
            return i;
        }
    }
}
