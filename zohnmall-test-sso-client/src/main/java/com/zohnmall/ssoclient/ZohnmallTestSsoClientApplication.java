package com.zohnmall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZohnmallTestSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallTestSsoClientApplication.class, args);
    }

}
