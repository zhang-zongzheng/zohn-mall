package com.zohnmall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZohnmallTestSsoClient2Application {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallTestSsoClient2Application.class, args);
    }

}
