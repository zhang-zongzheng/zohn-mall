package com.zohnmall.ssoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZohnmallTestSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallTestSsoServerApplication.class, args);
    }

}
