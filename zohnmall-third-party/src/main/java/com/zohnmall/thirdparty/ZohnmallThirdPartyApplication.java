package com.zohnmall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ZohnmallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZohnmallThirdPartyApplication.class, args);
    }

}
