package com.zohnmall.ware.dao;

import com.zohnmall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 04:11:00
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
