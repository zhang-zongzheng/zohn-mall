package com.zohnmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zohnmall.common.to.mq.OrderTo;
import com.zohnmall.common.to.mq.StockLockedTo;
import com.zohnmall.common.utils.PageUtils;
import com.zohnmall.ware.entity.WareSkuEntity;
import com.zohnmall.ware.vo.LockStockResultVo;
import com.zohnmall.ware.vo.SkuHasStockVo;
import com.zohnmall.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author zhangzongzheng
 * @email 751781196@qq.com
 * @date 2021-11-27 04:11:00
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVo vo);


    void unlockStock(StockLockedTo to);

    void unlockStock(OrderTo orderTo);
}

