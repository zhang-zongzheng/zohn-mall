package com.zohnmall.ware.vo;

import lombok.Data;

@Data
public class LockStockResultVo {
    private Long skuId; // 商品id
    private Integer num; // 数量
    private Boolean locked; //是否被锁定
}
